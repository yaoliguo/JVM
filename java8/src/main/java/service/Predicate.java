package service;

import java.util.ArrayList;
import java.util.List;

@FunctionalInterface
public interface Predicate<T> {

    boolean test(T t);

    static <T> List<T> filter(List<T> list, Predicate<T> p) {
        List<T> results = new ArrayList<>();
        for (T s : list) {
            if (p.test(s)) {
                results.add(s);
                System.out.println(s);
            }
        }
        return results;
    }

}
