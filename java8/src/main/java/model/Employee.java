package model;

public class Employee {

    private int no;

    private int age;

    private String name;

    public Employee() {
    }

    public Employee(int no, int age, String name) {
        this.no = no;
        this.age = age;
        this.name = name;
    }

    public int add(int i,int j){

        return i+j;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "no=" + no +
                ", age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
