package test;

import model.Employee;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestStream2 {

    public  List<Employee> query(Employee e){
        return createData();
    }

    public static List<Employee> createData(){

        List<Employee> emps = new LinkedList<>();
        emps.add(new Employee(1,20,"lisi"));
        emps.add(new Employee(2,21,"zhangsan"));
        emps.add(new Employee(3,22,"wangwu"));
        emps.add(new Employee(4,23,"zhaoliu"));
        emps.add(new Employee(5,24,"xiaoming"));
        emps.add(new Employee(5,29,"xiaoming"));
        emps.add(new Employee(5,28,"xiaoming"));
        emps.add(new Employee(6,27,"dali"));
        emps.add(new Employee(7,26,"libai"));
        emps.add(new Employee(8,27,"hedali"));
        emps.add(new Employee(9,28,"ergou"));
        emps.add(new Employee(10,29,"maoli"));

        return emps;
    }


    public static void main(String[] args) {
        List<Employee> datas = createData();
        TestStream2 t2 = new TestStream2();
        datas.parallelStream().map(m -> t2.query(m)).collect(Collectors.toMap((m)->m.get(0),(e)->e));
        datas.sort(Comparator.comparing(Employee::getNo).reversed().thenComparing(Employee::getAge));

        datas.get(0);

        Predicate<Employee> p = (m) -> m.getAge() > 27;
        Predicate<Employee> or = p.or(m->m.getAge() < 23);

        List<Employee> collect = datas.stream().filter(or).collect(Collectors.toList());

        collect.get(0);

    }

}
