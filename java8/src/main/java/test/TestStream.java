package test;

import model.Employee;
import service.Predicate;
import service.TestDefault;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestStream implements TestDefault {

    public static List<Employee> createData() {

        List<Employee> param = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Employee emp = new Employee(i, Math.getExponent(Math.random() * 100), "张三" + i);
            param.add(emp);
        }

        return param;

    }


    public static void stream() {

        //List<Employee> result = createData().stream().sorted((p1,p2) -> p1.getAge() > p2.getAge()).collect(Collectors.toList());
        //System.out.println(result);

    }

    public static void filter() {
        createData().stream()
                .filter(x -> {
                    System.out.println(x.getAge());
                    return x.getNo() > 5;
                }).collect(Collectors.toList()).forEach(System.out::println);
    }

    public static String show(String s) {

        return s;
    }

    public static void main(String[] args) {

        stream();
        filter();

        //接口默认实现
        new TestStream().show();

        List<Employee> list = Predicate.filter(createData(), p -> p.getAge() > 5);
        list.stream().filter(m -> m.getAge() == 1).flatMap( m ->new ArrayList<>().stream());

        int num = 10;

        Runnable run = () -> System.out.println(num);

        Consumer c = System.out::println;

        long i = 0l;

        System.out.println(i == 0l);

        String s = null;
        s = "";
        s += "123";
        System.out.println(s);


        Function<String, String> f = TestStream::show;

        System.out.println(f.apply("asdfsdfasdfasdf"));

        //Consumer<String> c = System.out::println;

        c.accept("asdfasdfasdfs");


    }

}
