import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Test {


    public static void main(String[] args) {

        Socket clent = null;
        OutputStream out = null;
        InputStream in = null;
        DataOutputStream dataOut = null;
        try {
            clent = new Socket("10.10.20.22", 6666);
            out = clent.getOutputStream();
            dataOut = new DataOutputStream(out);
            String name = "cert.zip";
            String path = "G:\\工作\\钥匙\\天辰\\";
            File file = new File(path + name);
            in = new FileInputStream(file);
            byte[] buf = new byte[1024];
            int lenth = 0;
            int total = 0;
            dataOut.writeUTF(name);
            while ((lenth = in.read(buf)) != -1) {
                dataOut.write(buf, 0, lenth);
                total += lenth;
                System.out.println("已经发送" + total + "字节");
                dataOut.flush();
            }
            clent.shutdownOutput();
            System.out.println("关闭输出流");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (dataOut != null) {
                try {
                    dataOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (clent != null) {
                try {
                    clent.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("全都关闭");

        }


    }

}
