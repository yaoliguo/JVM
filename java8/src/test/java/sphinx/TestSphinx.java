package sphinx;

public class TestSphinx {

    public static void main(String[] args) throws SphinxException {
        StringBuffer q = new StringBuffer();
        String host = "10.10.20.22";
        int port = 9312;
        int mode = SphinxClient.SPH_MATCH_ALL;
        String index = "*";
        int offset = 0;
        int limit = 20;
        int sortMode = SphinxClient.SPH_SORT_RELEVANCE;
        String sortClause = "";
        String groupBy = "";
        String groupSort = "";

        SphinxClient cl = new SphinxClient();
        cl.SetServer(host, port);
        cl.SetWeights(new int[]{100, 1});
        cl.SetMatchMode(mode);
        cl.SetLimits(offset, limit);
        cl.SetSortMode(sortMode, sortClause);
        if (groupBy.length() > 0)
            cl.SetGroupBy(groupBy, SphinxClient.SPH_GROUPBY_ATTR, groupSort);

        SphinxResult res = cl.Query(q.toString(), index);
    }

}
