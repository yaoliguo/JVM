package jvm;

public class Test01 {

    public static void main(String[] args) {

        /***
         * 不错的博客
         * https://www.cnblogs.com/redcreen/archive/2011/05/04/2037057.html
         * Xmx：堆最大内存  Xms：初始化堆堆存 Xmn：新生代大小
         * +UseParallelGC:使用并行收集器
         * PrintCommandLineFlags 打印第一行
         * PrintGCDetails 垃圾回收器工作时打印
         * -XX:+PrintHeapAtGC:打印GC前后的详细堆栈信息
         * NewRatio 年轻代(包括Eden和两个Survivor区)与年老代的比值(除去持久代)
         * SurvivorRatio Eden区与Survivor区的大小比值
         */
        //-Xmx10m -Xms5m -Xmn2m -XX:+UseParallelGC -XX:+PrintGCDetails -XX:+PrintCommandLineFlags
        //-Xmx10m -Xms5m -XX:NewRatio=2 -XX:+UseParallelGC -XX:SurvivorRatio=6 -XX:+PrintGCDetails -XX:+PrintCommandLineFlags
        System.out.println("空闲：" + Runtime.getRuntime().freeMemory() / 1024);
        System.out.println("最大：" + Runtime.getRuntime().maxMemory() / 1024);
        System.out.println("总共：" + Runtime.getRuntime().totalMemory() / 1024);

        byte[] buf = new byte[2 * 1024 * 1024];

        System.out.println("空闲：" + Runtime.getRuntime().freeMemory() / 1024);
        System.out.println("最大：" + Runtime.getRuntime().maxMemory() / 1024);
        System.out.println("总共：" + Runtime.getRuntime().totalMemory() / 1024);
    }

}
