var index = {
    ipArr: ["https://p.3.cn/prices/mgets?skuIds=J_GoodCode",
        "https://pe.3.cn/prices/pcpmgets?skuids=GoodCode&origin=5&area=15_1213_3409",
        "https://p.3.cn/prices/get?type=4&skuid=J_GoodCode",
        "https://pm.3.cn/prices/mgets?origin=2&skuIds=GoodCode"],
    sku: "7390561",
    jdPrice: 0
};

index.bindEvent = function () {

    $("#query").on("click", function () {
        index.request(0);
    });

}

index.request = function (i) {
    if (i == 4) {
        return;
    }
    index.sku = $("#jdsku").val();
    if (index.sku == '' || index.sku == null) {
        alert("请输入sku");
        return;
    }
    var url = index.ipArr[i].replace("GoodCode", index.sku);
    $.ajax({
        url: url,
        type: "get",
        async: false,
        dataType: "jsonp",
        success: function (msg) {
            if (msg.error == 'pdos_captcha') {
                index.request(i + 1);
                $("#showPrice").text(0);
            } else {
                index.jdPrice = msg[0].p;
                $("#showPrice").text(index.jdPrice);
                $("#showOrgPrice").text(msg[0].op);
                console.log(msg[0]);
            }

        }
    })
}

$(function () {
    index.bindEvent();
})