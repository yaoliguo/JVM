package http;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class HttpClient {
    public static void main(String[] args) throws IOException {

        HttpResponse response = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();

        HttpGet httpget = new HttpGet("https://p.3.cn/prices/mgets?skuIds=J_5724596");

        httpget.addHeader("X-Forwarded-For", "162.150.10.16");
        httpget.addHeader("Proxy-Client-IP", "162.150.10.16");
        httpget.addHeader("WL-Proxy-Client-IP", "162.150.10.16");
        httpget.setConfig(RequestConfig.DEFAULT);
        //httpget.setHeader("host","172.43.55.2");

        HttpResponse httpResponse = httpclient.execute(httpget);
        HttpEntity entity = httpResponse.getEntity();
        String s = EntityUtils.toString(entity);
        System.out.println(s);


        httpclient.close();


    }
}
